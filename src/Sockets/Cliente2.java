/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Sockets;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.*;
import java.io.*;


/**
 *
 * @author Luis
 */
public class Cliente2 extends JFrame implements ActionListener {

    Container c;
    JTextField op1,op2;
    JLabel l1,l2,l3;
    JPanel pN,pC,pS;
    JPanel p1,p2,p3,p4;
    JButton boton;
    JTextArea display;
    
    public Cliente2()
    {
         setTitle("Cliente de socket");
        setLayout(new GridLayout(4,1));
        c= getContentPane();
        c.setBackground(Color.blue);
        // creación de las intancias y agregación a los paneles 
        // de las componentes GUI
        // . . . . . . . .  . . . .
        Container contenedor=c;
        contenedor.setLayout(new FlowLayout());
        l1=new JLabel("Introduce porfavor los valores para:");
        contenedor.add(l1);
        
        
        l2=new JLabel("primer sumando:");
        contenedor.add(l2);
        op1=new JTextField(7);
        op1.addActionListener(this);
        contenedor.add(op1);
        
        l3=new JLabel("Segundo sumando");
        contenedor.add(l3);
        op2=new JTextField(5);
        op2.addActionListener(this);
        contenedor.add(op2);
        
        boton = new JButton("boton suma");
        contenedor.add(boton);
        
        display=new JTextArea(10,25);
        contenedor.add(display);
        
         pack();
        this.setLocationRelativeTo(null);
        setSize(300,350);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        boton.addActionListener(this);
        
    }         

 @Override
    public void actionPerformed(ActionEvent e) {
       double s1,s2;    
    	s1=Double.parseDouble(op1.getText());
    	s2=Double.parseDouble(op2.getText());
    	System.out.println("Haciendo petición");
    	peticionServidor(s1,s2);
    }
    
    private void peticionServidor(double s1, double s2)
    {
        // declaración de un objecto para el socket cliente
    Socket client;
    DataInputStream input;
    DataOutputStream output;
             // declaración de los objetos para el flujo de datos
         double suma;    
         String Suma;
         try {
                // creación de la instancia del socket
                    client = new Socket(InetAddress.getLocalHost(),6000);
		    display.setText("Socket Creado....\n");
                // creación de las instancias para el flujo de datos
                    input = new DataInputStream (client.getInputStream());
                    output = new DataOutputStream (client.getOutputStream());
	           display.append("Enviando primer sumando\n");
                  output.writeDouble(s1);
		     display.append("Enviando segundo sumando\n");
                  output.writeDouble(s2);
		     display.append ("El servidor dice....\n\n");
                  suma=input.readDouble();
                  Suma= String.valueOf (suma);
	           display.append("El  resultado es: "+ Suma+"\n\n");
                 display.append("Cerrando cliente\n\n");
                 client.close();
              }

                catch(IOException e){
                 e.printStackTrace();
                }
     }
    
    public static void main(String args[])
    {
        new Cliente2();
        
    }
}
